<?php include_once 'includes/helpers.inc.php'; ?>
<!doctype html>
<html>
<head>

<title>Original Wraps</title>
<meta charset="utf-8">

<meta name="description" content="">
<meta name="author" content="">

<!-- http://t.co/dKP3o1e -->
<meta name="HandheldFriendly" content="True">
<meta name="MobileOptimized" content="320">

<!-- For all browsers -->
<link rel="stylesheet" href="css/320andup.css">

<!--[if (lt IE 9) & (!IEMobile)]>
<script src="js/selectivizr-min.js"></script>
<![endif]-->

<!-- JavaScript -->
<script src="js/modernizr-2.5.3-min.js"></script>

<!-- Icons -->

<!-- 16x16 -->
<link rel="shortcut icon" href="/favicon.ico">
<!-- 32x32 -->
<link rel="shortcut icon" href="/favicon.png">
<!-- 57x57 (precomposed) for iPhone 3GS, pre-2011 iPod Touch and older Android devices -->
<!-- <link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png"> -->
<!-- 72x72 (precomposed) for 1st generation iPad, iPad 2 and iPad mini -->
<!-- <link rel="apple-touch-icon-precomposed" sizes="72x72" href="img/apple-touch-icon-72x72-precomposed.png"> -->
<!-- 114x114 (precomposed) for iPhone 4, 4S, 5 and post-2011 iPod Touch -->
<!-- <link rel="apple-touch-icon-precomposed" sizes="114x114" href="img/apple-touch-icon-114x114-precomposed.png"> -->
<!-- 144x144 (precomposed) for iPad 3rd and 4th generation -->
<!-- <link rel="apple-touch-icon-precomposed" sizes="144x144" href="img/apple-touch-icon-144x144-precomposed.png"> -->

<!--iOS -->
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<!-- <meta name="apple-mobile-web-app-title" content="320 and Up"> -->
<!-- <meta name="viewport" content="initial-scale=1.0"> (Use if apple-mobile-web-app-capable below is set to yes) -->
<!-- <meta name="apple-mobile-web-app-capable" content="yes"> -->
<!-- <meta name="apple-mobile-web-app-status-bar-style" content="black"> -->

<!-- Startup images -->

<!-- iPhone 3GS, pre-2011 iPod Touch -->
<!-- <link rel="apple-touch-startup-image" href="img/startup/startup-320x460.png" media="screen and (max-device-width:320px)"> -->
<!-- iPhone 4, 4S and 2011 iPod Touch-->
<!-- <link rel="apple-touch-startup-image" href="img/startup/startup-640x920.png" media="(max-device-width:480px) and (-webkit-min-device-pixel-ratio:2)"> -->
<!-- iPhone 5 and 2012 iPod Touch -->
<!-- <link rel="apple-touch-startup-image" href="img/startup/startup-640x1096.png" media="(max-device-width:548px) and (-webkit-min-device-pixel-ratio:2)"> -->
<!-- iPad landscape -->
<!-- <link rel="apple-touch-startup-image" sizes="1024x748" href="img/startup/startup-1024x748.png" media="screen and (min-device-width:481px) and (max-device-width:1024px) and (orientation:landscape)"> -->
<!-- iPad Portrait -->
<!-- <link rel="apple-touch-startup-image" sizes="768x1004" href="img/startup/startup-768x1004.png" media="screen and (min-device-width:481px) and (max-device-width:1024px) and (orientation:portrait)"> -->

<!-- Windows 8 / RT -->
<meta name="msapplication-TileImage" content="img/apple-touch-icon-144x144-precomposed.png">
<meta name="msapplication-TileColor" content="#000">
<meta http-equiv="cleartype" content="on">

</head>

<body>
<?php include 'includes/header.inc.php'; ?>

<div id = "content">
<center>
<h1>Original Wraps</h1>
<h2>Create XML for Automation</h2>
</center>

<form action="?" method="post">
<label for="Job Number">OW Number</label> 
<input type="text" name="ownumber" id="ownumber"/>

<label for="Vinyl Stock">Print Stock</label>
<select name="vinylstock" id="vinylstock">
			<option value="OW-IJ180-CV3 Vinyl/OW-8528 Lam">OW-IJ180-CV3 Vinyl/OW-8528 Lam</option>			
			<option value="OW-IJ180-CV3-60 Vinyl/OW-8528-60 Lam">OW-IJ180-CV3-60 Vinyl/OW-8528-60 Lam</option>			
			</option>
</select>
			
<label for="Cut Only Stock">Cut Only Stock</label>
			<select name="cutstock" id="cutstock">
			<option value="OW-1080 M12 Matte Black">OW-1080 M12 Matte Black</option>			
			<?php foreach ($owstocks as $owstock): ?>
			<option value="<?php htmlout($owstock['stock']); ?>"			
			<?php if ($owstock['id'] == $owstock)
			echo ' selected="selected"'; ?>>
				<?php htmlout($owstock['stock']); ?>
			</option>
			<?php endforeach; ?>
</select>

<label for="Premask">Premask</label>		
			<select name="premask" id="premask">
			<option selected="selected" value="OW-SCPM-44X Premask">OW-SCPM-44X Premask</option>
			<option value="OW-SCPS-55 Premask">OW-SCPS-55 Premask</option>			
			</select>

<label for="Who are you?">Who are you?</label>		
<select name="prepressoperator" id="prepressoperator">
			<option value="15">Select one</option>			
			<?php foreach ($prepressoperators as $prepressoperator): ?>
			<option value="<?php htmlout($prepressoperator['id']); ?>"			
			<?php if ($prepressoperator['id'] == $prepressoperatorid)
			echo ' selected="selected"'; ?>>
				<?php htmlout($prepressoperator['pfirstname']); ?> 
				<?php htmlout($prepressoperator['plastname']); ?>
			</option>
			<?php endforeach; ?>
</select>

<label for="Quantity">Quantity</label>
<input type="text" name="quantity" id="quantity" value="1"//>

<label for="File 1">File 1</label>
<input type="text" name="file1" id="file1">
<label for="File 2">File 2</label>
<input type="text" name="file2" id="file2"/>
<label for="File 3">File 3</label>
<input type="text" name="file3" id="file3"/>
<label for="File 4">File 4</label>
<input type="text" name="file4" id="file4"/>
<label for="File 5">File 5</label>
<input type="text" name="file5" id="file5"/>

<center>
<input type="submit" name="action" value="Submit"/>
</center>
</form>

</div>

</body>
</div>
</html>