<?php
include_once 'includes/magicquotes.inc.php';

// gettng started
if (isset($_POST['action']) and $_POST['action'] == 'Submit')
	{	
		include 'includes/connect.inc.php';
		$ownumber = mysqli_real_escape_string($link, $_POST['ownumber']);
		$vinylstock = mysqli_real_escape_string($link, $_POST['vinylstock']);
		$cutstock = mysqli_real_escape_string($link, $_POST['cutstock']);
		$premask = mysqli_real_escape_string($link, $_POST['premask']);
		$quantity = mysqli_real_escape_string($link, $_POST['quantity']);
		$prepressoperator = mysqli_real_escape_string($link, $_POST['prepressoperator']);
		$file1 = mysqli_real_escape_string($link, $_POST['file1']);
		$file2 = mysqli_real_escape_string($link, $_POST['file2']);
		$file3 = mysqli_real_escape_string($link, $_POST['file3']);
		$file4 = mysqli_real_escape_string($link, $_POST['file4']);
		$file5 = mysqli_real_escape_string($link, $_POST['file5']);
		


		
		//XML output
		$fp=fopen("XML_Output/".$ownumber.".xml","w+");
		fwrite($fp,"<?xml version=\"1.0\"?>\n<JDF>\n<JobNumber>".$ownumber."</JobNumber>\n<SixtyInchVinyl>OW-IJ180-CV3-60 Vinyl/OW-8528-60 Lam/".$premask."</SixtyInchVinyl>\n<VinylStock>".$vinylstock."/".$premask."</VinylStock>\n<PerfStock>OW-IJ8171 Perf/OW-8914 Lam</PerfStock>\n<CutOnlyStock>".$cutstock."/".$premask."</CutOnlyStock>\n<Operator>".$prepressoperator."</Operator>\n<Quantity>".$quantity."</Quantity>\n<FileSpec URL=".'"'.$file1.'"'."/>\n<FileSpec URL=".'"'.$file2.'"'."/>\n<FileSpec URL=".'"'.$file3.'"'."/>\n<FileSpec URL=".'"'.$file4.'"'."/>\n<FileSpec URL=".'"'.$file5.'"'."/>\n");
		fwrite($fp,"</JDF>");
		chmod("XML_Output/".$ownumber.".xml", 0777);
			
		header('Location: .');
		exit();
	}

// Build the list of prepress operators
include 'includes/connect.inc.php';
$sql = "SELECT id, pfirstname, plastname FROM prepress_operators ORDER BY pfirstname";

$result = mysqli_query($link, $sql);
if (!$result)
{
	$error = 'Error fetching list of operators.';
	include 'includes/error.html.php';
	exit();
}
while ($row = mysqli_fetch_array($result))
	{
	$prepressoperators[] = array(
	'id' => $row['id'],
	'pfirstname' => $row['pfirstname'],
	'plastname' => $row['plastname'],
	'selected' => FALSE);
	}
	
// Build the list of OW stocks
$sql = "SELECT id, stock FROM original_wrapssubstrate_list ORDER BY id";

$result = mysqli_query($link, $sql);
if (!$result)
{
	$error = 'Error fetching list of OW stocks.';
	include 'includes/error.html.php';
	exit();
}
while ($row = mysqli_fetch_array($result))
	{
	$owstocks[] = array(
	'id' => $row['id'],
	'stock' => $row['stock'],
	'selected' => FALSE);
	}
				
include 'enter.html.php';

?>